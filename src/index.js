const cards = [
    {
        src: "https://i.picsum.photos/id/241/300/300.jpg",
        header: "Mobile",
        paragraph: "Collaboratively administrate empowered markets via plug-and-play networks.",
        index: 0
    },
    {
        src: "https://i.picsum.photos/id/242/300/300.jpg",
        header: "TV",
        paragraph: "Dramatically engage seamlessly visualize quality intellectual capital without superior collaboration and idea-sharing.",
        index: 1
    },
    {
        src: "https://i.picsum.photos/id/243/300/300.jpg",
        header: "Tablet",
        paragraph: "Collaboratively administrate empowered markets via plug-and-play networks.",
        index: 2
    },
    {
        src: "https://i.picsum.photos/id/244/300/300.jpg",
        header: "iPhone",
        paragraph: "Collaboratively administrate empowered markets via plug-and-play networks.",
        index: 3
    },
    {
        src: "https://i.picsum.photos/id/248/300/300.jpg",
        header: "Piano",
        paragraph: "Collaboratively administrate empowered markets via plug-and-play networks.",
        index: 4
    },
    {
        src: "https://i.picsum.photos/id/247/300/300.jpg",
        header: "Software",
        paragraph: "Collaboratively administrate empowered markets via plug-and-play networks.",
        index: 5
    },
    {
        src: "https://i.picsum.photos/id/248/300/300.jpg",
        header: "Accounting Software",
        paragraph: "Collaboratively administrate empowered markets via plug-and-play networks.",
        index: 6
    },
    {
        src: "https://i.picsum.photos/id/249/300/300.jpg",
        header: "ERP",
        paragraph: "Collaboratively administrate empowered markets via plug-and-play networks.",
        index: 6
    },
    {
        src: "https://i.picsum.photos/id/250/300/300.jpg",
        header: "Keyboard",
        paragraph: "Collaboratively administrate empowered markets via plug-and-play networks.",
        index: 6
    },
    {
        src: "https://i.picsum.photos/id/251/300/300.jpg",
        header: "Mouse",
        paragraph: "Collaboratively administrate empowered markets via plug-and-play networks.",
        index: 6
    },
    {
        src: "https://i.picsum.photos/id/252/300/300.jpg",
        header: "Monitor",
        paragraph: "Collaboratively administrate empowered markets via plug-and-play networks.",
        index: 6
    },
    {
        src: "https://i.picsum.photos/id/253/300/300.jpg",
        header: "Laptap",
        paragraph: "Collaboratively administrate empowered markets via plug-and-play networks.",
        index: 6
    }
];
const basketItem = document.getElementById("basketItem");
const modal = document.getElementById("myModal");
const articles = document.getElementsByClassName("grid")[0];
const badgeShopping = document.getElementById("badge");
const span = document.getElementsByClassName("close")[0];
const basket = [];
let templates = '';
const basketGrid = document.getElementById('basketGrid');

span.onclick = () => {
    modal.style.display = "none";
};
window.onclick = function (event) {
    if (event.target === modal) {
        modal.style.display = "none";
    }
};

function createBasket() {
    let items = '';
    basket.forEach(card => {
        const template = `
            <article class="container-article">
                <img src="${card.src}" alt="Sample photo">
                <div class="text">
                    <h3>${card.header}</h3>
                    <p>${card.paragraph}</p>
                    <div class="container-count">
                        <div>Count</div>
                        <div>${card.count}</div>
                     </div>
                    <div class="container">
                        <button class="remove" data-count="${card.index}" style="height: 50px; width: 50px">
                            <i class="fa fa-trash" style="font-size: 30px"></i>
                        </button>
                    </div>
                </div>
            </article>
        `;
        items += template;
    });
    basketGrid.innerHTML = items;
    const buttons = Array.from(document.getElementsByClassName("remove"));
    buttons.forEach(button => {
        button.addEventListener("click", () => {
            const data = parseInt(button.dataset.count);
            const findCard = basket.findIndex(item => item.index === data);
            if (findCard > -1) {
                if (basket[findCard].count === 1) {
                    badgeShopping.innerHTML = parseInt(badgeShopping.innerHTML) - 1;
                    basket.splice(findCard, 1);
                } else {
                    basket[findCard].count--;
                }
                createBasket();
                if (basket.length === 0) {
                    modal.style.display = "none";
                }
            }
        });
    });
}

basketItem.addEventListener("click", () => {
    if (basket.length === 0) return;
    createBasket();
    modal.style.display = "block";
});

function createCardList() {
    cards.forEach(card => {
        const template = `
        <article class="container-article">
            <img src="${card.src}" alt="Sample photo">
            <div class="text">
                <h3>${card.header}</h3>
                <p>${card.paragraph}</p>
                <div class="container" style="margin-top: 5px">
                    <button class="plus" data-count="${card.index}">+</button>
                    <button class="minus" data-count="${card.index}">-</button>
                </div>
            </div>
        </article>
        `;
        templates += template;
    });
    articles.innerHTML = articles.innerHTML + templates;

    const buttons = Array.from(document.getElementsByClassName("plus"));
    buttons.forEach(button => {
        button.addEventListener("click", () => {
            const data = parseInt(button.dataset.count);
            const findCard = cards.find(item => item.index === data);
            const findInBasket = basket.find(item => item.index === data);
            if (findCard) {
                if (findInBasket) {
                    findInBasket.count++;
                } else {
                    const temp = {...findCard};
                    temp.count = 1;
                    basket.push(temp);
                    badgeShopping.innerHTML = parseInt(badgeShopping.innerHTML) + 1;
                }
            }
        });
    });

    const buttonsMinus = Array.from(document.getElementsByClassName("minus"));
    buttonsMinus.forEach(button => {
        button.addEventListener("click", () => {
            const data = parseInt(button.dataset.count);
            const findIndex = cards.findIndex(item => item.index === data);
            const findInBasket = basket.find(item => item.index === data);
            const findIndexBasket = basket.findIndex(item => item.index === data);
            if (findIndex) {
                const badge = parseInt(badgeShopping.innerHTML);
                debugger;
                if (badge > 0) {
                    if (findInBasket) {
                        if (findInBasket.count === 1) {
                            basket.splice(findIndexBasket, 1);
                            badgeShopping.innerHTML = (badge - 1).toString();
                        } else {
                            findInBasket.count--;
                        }
                    }
                }
            }
        });
    });
}

createCardList();


const nav = document.querySelector("#nav");
const closebtn = document.querySelector("#closeNav");
nav.addEventListener("click", () => {
    document.getElementById("mySidenav").style.width = "250px";
    myBackDrop.style.display = "flex";
});
closebtn.addEventListener("click", () => {
    document.getElementById("mySidenav").style.width = "0";
    myBackDrop.style.display = "none";
});

const myBackDrop = document.getElementsByClassName("openModal")[0];
myBackDrop.addEventListener("click", () => {
    myBackDrop.style.display = "none";
    document.getElementById("mySidenav").style.width = "0";
});
